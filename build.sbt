name := "vbot_node"

version := "1.0"

scalaVersion := "2.11.8"

resolvers += "Maven Central Server" at "http://repo1.maven.org/maven2"



libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.16"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.4.16"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "com.typesafe" % "config" % "1.3.1"
libraryDependencies += "com.sksamuel.elastic4s" %% "elastic4s-core" % "2.3.2"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"
libraryDependencies += "org.jsoup" % "jsoup" % "1.8.3"
libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.11.78"
libraryDependencies += "org.seleniumhq.selenium" % "selenium-chrome-driver" % "3.0.1"
libraryDependencies += "io.github.bonigarcia" % "webdrivermanager" % "1.5.1"



mainClass := Some("com.vbot_node.Main")