package com.vbot_node

import java.util
import java.util.UUID
import java.util.concurrent.{ThreadLocalRandom, TimeUnit}

import akka.actor.{Actor, Props}
import akka.actor.Actor.Receive
import com.vbot_node.DriverProtocol._
import com.vbot_node.TargetGeneratorProtocol.{GetRandomVideo, GetRandomVideoByAccountName, GetRandomVideoByKeywords, Video}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import com.vbot_node.UserAgentGeneratorProtocol.GetRandomUserAgent
import io.github.bonigarcia.wdm.ChromeDriverManager
import org.openqa.selenium.{By, JavascriptExecutor, WebElement}
import org.openqa.selenium.chrome.{ChromeDriver, ChromeOptions}

import scala.collection.immutable.Queue
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Random, Success}
import scala.collection.JavaConversions._
import scala.collection.JavaConverters.asScalaBufferConverter

/**
  * Created by xavi on 1/20/17.
  */
object DriverProtocol {

  case class InitDriver()

  case class TerminateDriver()

  case class Run()

  case class Search()

  case class LookForItems()

  case class WatchInvalidVideo()

  case class Back()

  case class KillDriver()


  case class WatchValidVideo()

}


object Driver {
  val TARGET_NUMBER = 20
  val MAX_DELAY_FOR_SEARCHING = 7000
  val MIN_DELAY_FOR_SEARCHING = 5000

  val INVALID_TARGET_MIN_TIME = 10000
  val INVALID_TARGET_MAX_TIME = 15000
  val VALID_TARGET_MIN_TIME = 40000
  val VALID_TARGET_MAX_TIME = 60000

  val MAX_INVALID_TARGETS = 3
}

class Driver extends Actor {
  implicit val timeout = Timeout(300 seconds)

  val targetGenerator = context.actorOf(Props[TargetGenerator])
  val userAgentGenerator = context.actorOf(Props[UserAgentGenerator])
  var driver: ChromeDriver = _
  var userAgent: String = _
  var video: Video = _
  var remainingInvalidTargets: Int = _
  var allowInvalid: Boolean = _

  def run(): Unit = {

    (targetGenerator ? GetRandomVideo()).onComplete({
      case Success(v: Video) =>
//        println(video)
        S3Logger.logToS3("selected_video", s"${v.name},${v.url},${v.accountName},${v.accountUrl}", UUID.randomUUID().toString)


        remainingInvalidTargets = Driver.MAX_INVALID_TARGETS
        allowInvalid = true
        video = v
        remainingInvalidTargets = Driver.MAX_INVALID_TARGETS

        driver.get("https://www.youtube.com")

        context.system.scheduler.scheduleOnce(
          Duration.create(ThreadLocalRandom.current().nextInt(Driver.MIN_DELAY_FOR_SEARCHING, Driver.MAX_DELAY_FOR_SEARCHING),
            TimeUnit.MILLISECONDS), self, Search())


      case Failure(e) =>
        e.printStackTrace()
    })

  }

  private def watchInvalidVideoAndReturn(driver: ChromeDriver) = {
    //    Thread.sleep(ThreadLocalRandom.current().nextInt(Driver.MIN_DELAY_FOR_SEARCHING, Driver.MAX_DELAY_FOR_SEARCHING))
    val searchItems = driver.findElements(By.className("yt-lockup-tile"))
    Random.shuffle(searchItems.asScala).head.click()
    Thread.sleep(ThreadLocalRandom.current().nextInt(Driver.INVALID_TARGET_MIN_TIME, Driver.INVALID_TARGET_MAX_TIME))
    driver.navigate().back()
    Thread.sleep(ThreadLocalRandom.current().nextInt(Driver.MIN_DELAY_FOR_SEARCHING, Driver.MAX_DELAY_FOR_SEARCHING))
  }

  private def getFoundElements(searchItems: util.List[WebElement], video: Video) = {
    //    println(searchItems.size(), "'''''")
    //    driver.findElements(By.className("yt-lockup-tile"))

    searchItems.asScala.filter(webElement => {
      //      println(webElement.findElement(By.cssSelector(".yt-lockup-title a"))
      //        .getAttribute("title").trim.toLowerCase, webElement.findElement(By.cssSelector(".yt-lockup-byline a")).getText,
      //        video.name.trim.toLowerCase, video.accountName


      webElement.findElement(By.cssSelector(".yt-lockup-title a"))
        .getAttribute("title").trim.toLowerCase == video.name.trim.toLowerCase &&
        webElement.findElement(By.cssSelector(".yt-lockup-byline a")).getText.trim.toLowerCase == video.accountName.trim.toLowerCase
    })
  }

  def initDriver() = {
    println("Driver", self)
    (userAgentGenerator ? GetRandomUserAgent()).onComplete({

      case Success(ua) =>

        userAgent = ua.toString
        println(userAgent)
        val chromeOptions = new ChromeOptions()
        chromeOptions.addArguments(userAgent.toString)
        chromeOptions.addArguments("--verbose --whitelisted-ips=172.29.104.93")
        chromeOptions.addArguments("--no-sandbox");
        ChromeDriverManager.getInstance().setup()
        driver = new ChromeDriver(chromeOptions)

        self ! Run()
      case Failure(e) =>
        e.printStackTrace()
    })

  }


  def search() = {
    val searchBox = driver.findElement(By.id("masthead-search-term"))
    searchBox.sendKeys(video.name)
    searchBox.submit()
    context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(Driver.MIN_DELAY_FOR_SEARCHING, Driver.MAX_DELAY_FOR_SEARCHING),
      TimeUnit.MILLISECONDS), self, LookForItems())
  }

  def lookForItems() = {
    val searchItems = driver.findElements(By.className("yt-lockup-tile"))
    //    println("'''''",searchItems)
    var foundElements = getFoundElements(searchItems, video)

    //    println(foundElements)


    if (foundElements.nonEmpty) {
      allowInvalid = true

      //      allowInvalid = true
      if (remainingInvalidTargets > 0) {
        println(self.path, "Encontro el elemento pero faltan intentos")
        context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(Driver.MIN_DELAY_FOR_SEARCHING, Driver.MAX_DELAY_FOR_SEARCHING),
          TimeUnit.MILLISECONDS), self, WatchInvalidVideo())
      }
      else {
        println(self.path, "-------------------------- Encontro el elemento ")
        foundElements.head.click()
        context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(Driver.VALID_TARGET_MIN_TIME, Driver.VALID_TARGET_MAX_TIME),
          TimeUnit.MILLISECONDS), self, Run())
        //        remainingInvalidTargets = Driver.MAX_INVALID_TARGETS
      }


    } else if (allowInvalid) {
      println(self.path, "No encontro el elemento y va a ver uno invalido")
      allowInvalid = false
      context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(Driver.MIN_DELAY_FOR_SEARCHING, Driver.MAX_DELAY_FOR_SEARCHING),
        TimeUnit.MILLISECONDS), self, WatchInvalidVideo())
    } else {
      allowInvalid = true
      println(self.path, "No encontro el elemento y debe pasar de pagina")
      driver.findElement(By.cssSelector(s".yt-uix-button[aria-label='Go to page ${Driver.MAX_INVALID_TARGETS - remainingInvalidTargets + 1}']")).click()
      remainingInvalidTargets -= 1
      context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(Driver.MIN_DELAY_FOR_SEARCHING, Driver.MAX_DELAY_FOR_SEARCHING),
        TimeUnit.MILLISECONDS), self, LookForItems())

    }


  }

  def watchInvalidVideo() = {
    val searchItems = driver.findElements(By.className("yt-lockup-tile"))
    if(searchItems.isEmpty){
      self ! Run()
    }else{
      Random.shuffle(searchItems.asScala).head.click()

      remainingInvalidTargets -= 1
      context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(Driver.INVALID_TARGET_MIN_TIME, Driver.INVALID_TARGET_MAX_TIME),
        TimeUnit.MILLISECONDS), self, Back())
    }

  }

  def back() = {
    S3Logger.logToS3("watched_videos", s"${driver.getCurrentUrl}", UUID.randomUUID().toString)
    driver.navigate().back()

    context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(Driver.MIN_DELAY_FOR_SEARCHING, Driver.MAX_DELAY_FOR_SEARCHING),
      TimeUnit.MILLISECONDS), self, LookForItems())
//    self ! LookForItems()
    //    context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(Driver.MIN_DELAY_FOR_SEARCHING, Driver.MAX_DELAY_FOR_SEARCHING),
    //      TimeUnit.MILLISECONDS), self, LookForItems())
  }


  def watchValidVideo() = {

  }

  override def receive: Receive = {
    case InitDriver() => initDriver()
    case Run() => run()
    case TerminateDriver() => driver.close()
    case Search() => search()
    case LookForItems() => lookForItems()
    case WatchInvalidVideo() => watchInvalidVideo()
    case Back() => back()
    case WatchValidVideo() => watchValidVideo()
//    case KillDriver
  }
}
