package com.vbot

import akka.actor.{ActorSystem, Props}
import akka.event.Logging
import com.vbot_node.DriverManager
import com.vbot_node.DriverManagerProtocol.Init

/**
  * Created by vbot on 1/13/17.
  */
object Main extends App {


  val system = ActorSystem("vbot_node")
  system.actorOf(Props[DriverManager]) ! Init()


  /*
  0=accessKeyId
  1=secretKeyId
  2=bitbucketPassword
   */
  if (this.args.length == 3) {

  }

}