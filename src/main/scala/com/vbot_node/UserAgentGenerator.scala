package com.vbot_node

import java.io.{BufferedReader, InputStreamReader}
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util
import java.util.concurrent.ThreadLocalRandom

import akka.actor.Actor
import akka.actor.Actor.Receive
import com.amazonaws.auth.{AWSStaticCredentialsProvider, BasicAWSCredentials}
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.GetObjectRequest
import com.typesafe.config.ConfigFactory
import com.vbot_node.UserAgentGeneratorProtocol.GetRandomUserAgent


/**
  * Created by vbot on 1/21/17.
  */

object UserAgentGeneratorProtocol {

  case class GetRandomUserAgent()

}

object UserAgentGenerator {

  val BUCKET = "vlogactivity"
  val DEFAULT_CSV = "ua/default.txt"
  val DEFAULT_UA = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
  val SEPARATOR = ":::"
}

class UserAgentGenerator() extends Actor {


  val accessKeyId: String = ConfigFactory.load().getString("aws.accessKeyId")
  val secretKeyId: String = ConfigFactory.load().getString("aws.secretKeyId")


  def generateUAPool() {
    val awsCreds = new BasicAWSCredentials(accessKeyId, secretKeyId)
    val s3Client = AmazonS3ClientBuilder.standard()
      .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
      .withRegion(Regions.US_EAST_1)
      .build()
    val s3Object = try {
      s3Client.getObject(
        new GetObjectRequest(UserAgentGenerator.BUCKET, "ua/" + getCurrentKey + ".txt"))
    } catch {
      //      case foo: AmazonS3Exception => handleFooException(foo)
      case _: Throwable => s3Client.getObject(
        new GetObjectRequest(UserAgentGenerator.BUCKET, UserAgentGenerator.DEFAULT_CSV))
    }

    val reader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent))
    var line: String = reader.readLine()
    var count = 0
    var uaMap: Map[Int, String] = Map()
    while (line != null) {
      uaMap = uaMap + (count -> line.split(UserAgentGenerator.SEPARATOR)(0))
      count += 1
      line = reader.readLine()
    }
    uaPool = (getCurrentKey, uaMap)
  }

  var uaPool: (String, Map[Int, String]) = ("", Map())

  def getRandomUserAgent() {
    val key = getCurrentKey
    if (uaPool._1 != key) {
      generateUAPool()
    }
    sender() ! uaPool._2.getOrElse(ThreadLocalRandom.current().nextInt(0, uaPool._2.size),
      UserAgentGenerator.DEFAULT_UA)

  }

  private def getCurrentKey: String = {
    LocalDate.now().format(DateTimeFormatter.ofPattern("yy-MM-dd"))
  }

  override def receive: Receive = {
    case GetRandomUserAgent() => getRandomUserAgent()
  }
}
