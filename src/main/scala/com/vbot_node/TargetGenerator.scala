package com.vbot_node


import java.util.concurrent.ThreadLocalRandom

import akka.pattern.{ask, pipe}
import akka.actor.Actor
import akka.actor.Actor.Receive
import akka.event.Logging
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.{ElasticDsl, RichGetResponse}
import com.vbot_node.TargetGeneratorProtocol.{GetRandomVideo, GetRandomVideoByAccountName, GetRandomVideoByKeywords, Video}

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Random, Success}

/**
  * Created by vbot on 1/23/17.
  */

object TargetGeneratorProtocol {

  case class Video(name: String, url: String, accountName: String, accountUrl: String, keywords: Seq[String],
                   seconds: Int, createdAt: Long)

  case class GetRandomVideo()

  case class GetRandomVideoByKeywords(video: Video)

  case class GetRandomVideoByAccountName(video: Video)

}

object TargetGenerator {
  val DEFAULT_VIDEO = Video("Tanto ganas tanto pierdes (Darío Castro cover)",
    "https://www.youtube.com/watch?v=RzakOcQZ6BA",
    "Xavier Ramírez Espinoza",
    "https://www.youtube.com/channel/UCIZTHE-3WEZixBirbt9srmg",
    Seq("Tanto ganas tanto pierdes", "Darío Castro"),
    249, 1485263505)
  val LIMIT_PER_QUERY = 500
}


class TargetGenerator extends Actor {
  val log = Logging(context.system, this)

  def convertToVideo(map: java.util.Map[String, AnyRef]): Video = {

    try {
      Video(map.get("name").toString,
        map.get("url").toString,
        map.get("accountName").toString,
        map.get("accountUrl").toString,
        map.get("keywords").asInstanceOf[java.util.List[String]].toList,
        map.get("seconds").asInstanceOf[Int],
        map.get("createdAt").asInstanceOf[Int]
      )
    } catch {
      case _ => TargetGenerator.DEFAULT_VIDEO
    }

  }

  private def getRandomNameByKeywords(video: Video) = {

    val keyword = if (video.keywords.size > 1) {
      video.keywords(ThreadLocalRandom.current().nextInt(0, video.keywords.size - 1))
    } else {
      video.keywords.head
    }


    ElasticsearchClient.client.execute {
      search in "videos" -> "default" query {
        termQuery("keywords", keyword)
      } size TargetGenerator.LIMIT_PER_QUERY
    }.onComplete {
      case Success(richSearchResponse) if richSearchResponse.totalHits > 1 =>
        sender() ! convertToVideo(Random.shuffle(richSearchResponse.hits.toList).head.sourceAsMap)
      case Success(x) =>
        getRandomVideo()
      case Failure(e) =>
        e.printStackTrace()

    }
  }

  private def getRandomVideoByAccountName(video: Video) = {
    ElasticsearchClient.client.execute {
      search in "videos" -> "default" query video.accountName size TargetGenerator.LIMIT_PER_QUERY
    }.onComplete {
      case Success(richSearchResponse) if richSearchResponse.totalHits > 1 =>
        sender() ! convertToVideo(Random.shuffle(richSearchResponse.hits.toList).head.sourceAsMap)
      case Success(_) =>
        getRandomVideo()
      case Failure(e) =>
        e.printStackTrace()

    }
  }

  private def getRandomVideo() = {
    val previousSender = sender()
    ElasticsearchClient.client.execute {
      search in "videos" -> "default" size 0
    }.map(_.totalHits.toInt)
      .flatMap(length => {
        println("///////", ThreadLocalRandom.current().nextInt(1, length))
        ElasticsearchClient.client.execute {
          get id ThreadLocalRandom.current().nextInt(1, length).toString from "videos" -> "default"
        }
      }).onComplete {
      case Success(richGetResponse) =>
        println("Success", convertToVideo(richGetResponse.source))
        println(previousSender)
        previousSender ! convertToVideo(richGetResponse.source)
      //      case Success(_) =>
      //        getRandomVideo()
      case Failure(e) =>

        e.printStackTrace()

    }

    //      .map(richGetResponse => convertToVideo(richGetResponse.source)) pipeTo sender()
  }

  override def receive: Receive = {
    case GetRandomVideo() =>
      getRandomVideo()

    case GetRandomVideoByAccountName(video) =>
      getRandomVideoByAccountName(video)

    case GetRandomVideoByKeywords(video) =>
      getRandomNameByKeywords(video)
  }


}
