package com.vbot_node

import com.sksamuel.elastic4s.{ElasticClient, ElasticsearchClientUri}

/**
  * Created by vbot on 1/23/17.
  */
object ElasticsearchClient {

  val HOST = "ec2-34-197-125-65.compute-1.amazonaws.com"
  val PORT = 9300
  val client = ElasticClient.transport(ElasticsearchClientUri(HOST, PORT))
}
