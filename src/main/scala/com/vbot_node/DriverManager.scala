package com.vbot_node

import java.lang.management.ManagementFactory
import java.lang.reflect.Modifier
import java.util.concurrent.TimeUnit

import scala.concurrent.ExecutionContext.Implicits.global
import collection.JavaConverters._
import akka.actor.{Actor, ActorRef, OneForOneStrategy, Props}
import akka.actor.Actor.Receive
import akka.actor.SupervisorStrategy.Resume
import com.typesafe.config.ConfigFactory
import com.vbot_node.DriverManagerProtocol.{ForceCreation, Init, Tick}
import com.sun.management.OperatingSystemMXBean
import com.vbot_node.DriverProtocol.{InitDriver, Run, TerminateDriver}

import scala.concurrent.duration._
import scala.collection.immutable.Queue
import scala.concurrent.duration.Duration

/**
  * Created by xavi on 1/20/17.
  */
object DriverManagerProtocol {

  case class Init()

  case class Tick()

  case class ForceCreation()

}

object DriverManager {
}

class DriverManager extends Actor {


  private val checkSchedulerTime = ConfigFactory.load().getLong("driverManager.checkSchedulerTime")
  private var drivers: Queue[ActorRef] = _
  //  private val operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean.getA
  private val maxCPUUsagePercentage = ConfigFactory.load().getInt("driverManager.maxCPUUsagePercentage")
  private val minFreeMemoryMB = ConfigFactory.load().getLong("driverManager.minFreeMemoryMB")
  private var dequeuedDriver: ActorRef = _


  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 minute) {
      //      case _: ArithmeticException      => Resume
      //      case _: NullPointerException     => Restart
      //      case _: IllegalArgumentException => Stop
      case _: Exception =>
        sender() ! Run()
        Resume
    }


  def getFreeMemoryUsageMB: Int = {
    ManagementFactory.getOperatingSystemMXBean match {
      case x: com.sun.management.OperatingSystemMXBean =>
        (x.getFreePhysicalMemorySize / (1024 * 1024)).toInt
      case _ => 0
    }
  }

  def getCPUUsagePercentage: Int = {
    ManagementFactory.getOperatingSystemMXBean match {
      case x: com.sun.management.OperatingSystemMXBean =>
        (x.getSystemCpuLoad * 100).toInt
      case _ => 100
    }
  }


  def tick() = {
    if (getFreeMemoryUsageMB < minFreeMemoryMB || getCPUUsagePercentage > maxCPUUsagePercentage) {
      val deQueued = drivers.dequeue
      deQueued._1 ! TerminateDriver()
    } else {
      val newDriver = context.actorOf(Props[Driver])
      newDriver ! InitDriver()
      drivers.enqueue(newDriver)
    }

  }

  def forceCreation() = {
    (1 to 4).foreach(_ => {
      val newDriver = context.actorOf(Props[Driver])
      context.watch(newDriver)
      newDriver ! InitDriver()
      drivers.enqueue(newDriver)
      //    Thread.sleep(120000)
      //    val newDriver2 = context.actorOf(Props[Driver])
      //    newDriver2 ! InitDriver()
      //    drivers.enqueue(newDriver2)
    })
  }


  override def receive: Receive = {
    case Init() => init()
    case Tick() => tick()
    case ForceCreation() => forceCreation()
  }

  private def init() = {
    drivers = Queue.empty
    self ! ForceCreation()
    //    self ! Tick()
    //    self ! Tick()

    //    context.system.scheduler.schedule(Duration.create(checkSchedulerTime, TimeUnit.MILLISECONDS),
    //      Duration.create(checkSchedulerTime, TimeUnit.MILLISECONDS), self, Tick())
  }
}
